<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Etudiant;
use App\Entity\Examen;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

final class NoteAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
			->add('valeur')
			;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('etudiant.nom')
            ->add('etudiant.prenom')
            ->add('examen.libelle')
            ->add('valeur',null,[
                'label' => 'Note'
            ])
			->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
			->add('valeur')
            ->add('etudiant',EntityType::class, array(
                'placeholder' => 'Choose an option',
                'class' => Etudiant::class,
                'choice_label' => function (Etudiant $etudiant) {
                    return $etudiant->getNom() . ' ' . $etudiant->getPrenom();
                },
            ))
            ->add('examen',EntityType::class, array(
                'placeholder' => 'Choose an option',
                'class' => Examen::class,
                'choice_label' => "libelle" ,
            ))
			;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
			->add('valeur')
			;
    }
}
