<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use App\Entity\Etudiant;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

final class SeanceAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
			->add('id')
			;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
			->add('id')
			->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('etudiants', EntityType::class, [ // add this
                'class'        => Etudiant::class,
                'choice_label' => function (Etudiant $Etudiant) {
                    return $Etudiant->getNom() . ' ' . $Etudiant->getPrenom();
                },
                'label'        => 'Who is fighting in this round?',
                'expanded'     => true,
                'multiple'     => true,
            ])
			;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
			->add('id')
			;
    }
}
