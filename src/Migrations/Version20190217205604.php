<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190217205604 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE seance_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE seance (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE seance_etudiant (seance_id INT NOT NULL, etudiant_id INT NOT NULL, PRIMARY KEY(seance_id, etudiant_id))');
        $this->addSql('CREATE INDEX IDX_9DC5A29BE3797A94 ON seance_etudiant (seance_id)');
        $this->addSql('CREATE INDEX IDX_9DC5A29BDDEAB1A3 ON seance_etudiant (etudiant_id)');
        $this->addSql('ALTER TABLE seance_etudiant ADD CONSTRAINT FK_9DC5A29BE3797A94 FOREIGN KEY (seance_id) REFERENCES seance (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE seance_etudiant ADD CONSTRAINT FK_9DC5A29BDDEAB1A3 FOREIGN KEY (etudiant_id) REFERENCES etudiant (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE seance_etudiant DROP CONSTRAINT FK_9DC5A29BE3797A94');
        $this->addSql('DROP SEQUENCE seance_id_seq CASCADE');
        $this->addSql('DROP TABLE seance');
        $this->addSql('DROP TABLE seance_etudiant');
    }
}
